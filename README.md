# README #

This README would normally document whatever steps are necessary to get your application up and running.

### How to set up? ###

* Make sure NodeJS, MongoDB is installed.
* Clone the repository.
* Go inside the frontend folder and in terminal (cmd) execute "npm install" command.
* Go inside the backend folder and in terminal (cmd) execute "npm install" command.
* Go inside the frontend folder and build the application using "npm run build" command.
  It will generate a build folder once the build is successful.
* Copy the build folder and go to backend folder and paste it.
* Once done execute "npm start" command in the backend folder.
* Once the above command is executed the application will be up and running.
* Visit localhost:3000 to see the application.
