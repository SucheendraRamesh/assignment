'use strict';
const accService   = require('../Services/AccessManagementService');
const utils        = require('../Utils/Utility');
const httpMsgs     = require('../Utils/HttpMsgConsts');

const insertData = (req, res) => {
        // console.log('Data =>',req.body);
  // get user whose email id is sent in Body. Even if we get back ID that means we have the user.
    accService.checkIfDataExists(req.body, { _id: 1}, (exists) => { 
    //   console.log('isExists ???? ',exists);
	if (exists) {
		// console.log('Data Exists! ');
	res.status(httpMsgs.HTTP_ERR_CODE.HTTP_BAD_REQ).send({ errors: ['User already exists.']});
		return;
    }
    else{
        accService.insertData(req.body, (err, result) => {
            // console.log('data doesnt exist. proceed to insert it to DB');
            if (err) {
                console.log(err);
            } else {
                //console.log('The result controller is ',result);
                res.send(result);
            }
        });
     }
    })
        
 }


 const getAllData = (req, res) => {
    ////console.log('Inside getData method');
    accService.getAllData({}, (err, result) => {
		if (err) {
			utils.sendResponse(res, err);
		} else {
            ////console.log('The result controller is ',result);
            utils.sendResponse(res, null, result);
		}
	});
}

const deleteData = (req, res) => {
    let aid = +req.params.aid;
    // console.log('ID FOR DELETION ',aid);
    accService.deleteData(aid, (err, result) => {
        if (err) {
           utils.sendResponse(res, err);
        } else {
            ////console.log('The result controller is ',result);
            utils.sendResponse(res, null, result);
        }
        });

}



 
 module.exports.insertData = insertData;
 module.exports.getAllData = getAllData;     
 module.exports.deleteData = deleteData;