'use strict';
const dmService   = require('../Services/DefinitionManagementService');
const httpMsgs    = require('../Utils/HttpMsgConsts');

const insertData = (req, res) => {

    // console.log('DATA ==> ',req.body);
    // get user whose details are sent in Body. Even if we get back ID that means we have the user.
    dmService.checkIfUserExists(req.body, { _id: 1},(exists) => { 
        // console.log('isExist???? ',exists);
      if (exists) {
        //   console.log('User with this data already exists. ');
            // res.send({ errors: ['User Already Exists']}).status(httpMsgs.HTTP_ERR_CODE.HTTP_BAD_REQ);
             res.status(httpMsgs.HTTP_ERR_CODE.HTTP_BAD_REQ).send({ errors: ['User already exists.']});
                return;
      }
      else{
        // console.log('User Doesnt exist.. proceed to insert to DB');
        dmService.insertData(req.body, (err, result) => {
            if (err) {
                console.log(err);
            } else {
                //console.log('The result controller is ',result);
                res.send(result);
            }
        });
      }
    })
  
 }
 
 module.exports.insertData = insertData;