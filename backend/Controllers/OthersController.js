'use strict';
const othService   = require('../Services/OthersManagementService');


const insertData = (req, res) => {
        // console.log('OthersController', req.body);
        othService.insertData(req.body, (err, result) => {
         if (err) {
             console.log(err);
         } else {
             //console.log('The result controller is ',result);
             res.send(result);
         }
     });
 }
 
 module.exports.insertData = insertData;