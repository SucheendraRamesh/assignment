'use strict';
const definition   = require('../Schemas/DefinitionManagementSchema');
const userAccess   = require('../Schemas/AccessManagementSchema');
const other        = require('../Schemas/OthersManagementSchema');
const mongoose     = require('mongoose');
const httpMsgs     = require('../Utils/HttpMsgConsts');

var model;

const setModel = (modelType) => {
  if(modelType=='Definition')
    model = definition;
  else if(modelType=='userAccess')
     model = userAccess;
  else if(modelType=='Others')
    model = other;
}
 

const putData = (data, done) => 
 {
  const newObject = new model(data);
  //console.log('schema is ',schema);
  newObject.save(function(err) {
    if (err){
       done({isError: true, eMsg: httpMsgs.HTTP_ERR_MSGS.INT_SRV_ERR, 
        eCode: httpMsgs.HTTP_ERR_CODE.HTTP_IN_SERR});
    } 
    else{
      done(null,{code: httpMsgs.HTTP_ERR_CODE.HTTP_OK, 
        msg: 'Data inserted Successfully'});  
    }
    
   });
 }

 const deleteData = (select, done) => {
  model.deleteOne(select, (err, data) => {
    if(err){
      done({isError: true, eMsg: httpMsgs.HTTP_ERR_MSGS.INT_SRV_ERR, 
          eCode: httpMsgs.HTTP_ERR_CODE.HTTP_IN_SERR});
    }
    else{
      done(null,{code: httpMsgs.HTTP_ERR_CODE.HTTP_OK, 
          msg: 'Data Successfully deleted !!',data:{object:data}});
    }
    });
 }

 const getAllData = (filter, projection='', done) => {
  model.find(filter, projection, (err, data) => { 
  if(err){
       done({isError: true, eMsg: httpMsgs.HTTP_ERR_MSGS.INT_SRV_ERR, 
        eCode: httpMsgs.HTTP_ERR_CODE.HTTP_IN_SERR});
  }
  else{
    done(null,{code: httpMsgs.HTTP_ERR_CODE.HTTP_OK, 
        msg: 'Data successfully Fetched!!',data:{object:data}});
  }
  });
}



module.exports.setModel = setModel;
module.exports.getAllData = getAllData;
module.exports.putData = putData;
module.exports.deleteData = deleteData;