const mongoose = require('mongoose');


/**
 * This file is specifically to get the connection to the db 
 * and creating all the schemas that are required
 */

const Schema = mongoose.Schema;

const userAccessSchema = new Schema({
  aid: Number,
  isRoot: Boolean,
  isArchitect: Boolean,
  isDeveloper: Boolean,
  isSupportEngineer: Boolean,
  isProducer: Boolean,
  userType: String,
  name: String
});

// the schema is useless so far
// we need to create a model using it
const accessManagement = mongoose.model('userAccess', userAccessSchema);

// make this available to our users in our Node applications
module.exports = accessManagement;