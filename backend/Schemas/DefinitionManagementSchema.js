const mongoose = require('mongoose');


/**
 * This file is specifically to get the connection to the db 
 * and creating all the schemas that are required
 */

const Schema = mongoose.Schema;

// create a brand schema
const definitionSchema = new Schema({
  did: Number,
  applicationName:String,
  division: String,
  stream: String,
  type: String,
  startDate: Date,
  endDate: Date,
  financialSwitch: Boolean
  
});

// the schema is useless so far
// we need to create a model using it
const definitionManagement = mongoose.model('definitions', definitionSchema);

// make this available to our users in our Node applications
module.exports = definitionManagement;