const mongoose = require('mongoose');


/**
 * This file is specifically to get the connection to the db 
 * and creating all the schemas that are required
 */

const Schema = mongoose.Schema;

// create a brand schema
const otherSchema = new Schema({
  otherData: String,
  oid: Number
  
});

// the schema is useless so far
// we need to create a model using it
const otherManagement = mongoose.model('others', otherSchema);

// make this available to our users in our Node applications
module.exports = otherManagement;