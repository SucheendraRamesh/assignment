const superModel = require('../Models/XManagementModel');
const utils = require('../Utils/Utility');

const insertData = (data, done) => {
    // console.log('Inside insertData() Service',data);
    data['aid'] = utils.getUniqueId();
    superModel.setModel('userAccess');  
    superModel.putData(data,done);
   
}

const checkIfDataExists = (userInfo, proj='', done)=>{
    superModel.setModel('userAccess');
    let select = {};
    
    select['isRoot'] = userInfo['isRoot'];
    select['isArchitect'] = userInfo['isArchitect'];
    select['isDeveloper'] = userInfo['isDeveloper'];	
    select['isSupportEngineer'] = userInfo['isSupportEngineer'];
    select['isProducer'] = userInfo['isProducer'];
    select['userType'] = userInfo['userType'];
    select['name'] = userInfo['name'];

    // console.log('Select query ',select);

    superModel.getAllData(select, proj, (err,res) => {
    //   console.log('Res ',res['data']['object']);
      if(err){
        done(false);
      }else{
        if(res['data']['object'][0]==null || res['data']['object'][0]['_id'] == null)
          done(false); // User not there.
        else
          done(true);
      }
    });
  }
  


const getAllData = (sel, done) => {
    // console.log('Inside getAllData() Service');
    superModel.setModel('userAccess');    
    superModel.getAllData(sel,'-_id -__v', done);
}


const deleteData = (id, done) => {
    let select = {};
    select['aid'] = id;
    //console.log('VID is ' +id);
    ////console.log('Printing select ',select);
    ////console.log('VID is ' +vid);
    superModel.setModel('userAccess');
    superModel.deleteData(select, done);

}
module.exports.insertData = insertData;
module.exports.getAllData = getAllData;
module.exports.deleteData = deleteData;
module.exports.checkIfDataExists = checkIfDataExists;