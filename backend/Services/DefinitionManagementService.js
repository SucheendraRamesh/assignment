const superModel = require('../Models/XManagementModel');
const utils = require('../Utils/Utility');

const insertData = (data, done) => {
    //console.log('Inside insertData() Service');
    data['did'] = utils.getUniqueId();
    superModel.setModel('Definition');  
    superModel.putData(data,done);
   
}


const checkIfUserExists = (userInfo, proj='', done) => {
    console.log('checkIfUserExists Service ',userInfo);
    let select = {};
    select['applicationName'] = userInfo['applicationName'];
    select['division'] = userInfo['division'];
    select['stream'] = userInfo['stream'];
    select['type'] = userInfo['type'];
    select['startDate'] = userInfo['startDate'];
    select['endDate'] = userInfo['endDate'];
    select['financialSwitch'] = userInfo['financialSwitch'];
    console.log('select ',select);
    superModel.setModel('Definition');	
    superModel.getAllData(select, proj, (err ,res) => {
    //   console.log('Res ',res['data']['object'][0]);
      if(err){
        done(false);
      }else{
        if(res['data']['object'][0] == null || res['data']['object'][0]['_id'] == null)
          done(false); // User not there.
        else
          done(true);
      }
    });
  }
  


module.exports.insertData = insertData;
module.exports.checkIfUserExists = checkIfUserExists;