const superModel = require('../Models/XManagementModel');
const utils = require('../Utils/Utility');

const insertData = (data, done) => {
    //console.log('Inside insertData() Service');
    data['oid'] = utils.getUniqueId();
    superModel.setModel('Others');  
    superModel.putData(data,done);
   
}

module.exports.insertData = insertData;