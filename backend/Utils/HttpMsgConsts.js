'use strict'
module.exports = {
	HTTP_ERR_CODE : {
		HTTP_OK 		: 200, // 200 OK
		HTTP_MUL 		: 300, // Multiple Choices
		HTTP_MOV_PERM 	: 301, // Moved Permanently
		HTTP_FOUND 		: 302, // Found (The requested resource resides temporarily under a different URI)
		HTTP_NOT_MOD 	: 304, // Not Modified
		HTTP_TMP_REDI 	: 307, // Temporary Redirect
		HTTP_BAD_REQ 	: 400, // Bad Request
		HTTP_UN_AUTH 	: 401, // Unauthorized
		HTTP_FORBIDN 	: 403, // Forbidden
		HTTP_NOT_FOUND 	: 404, // Not Found
		HTTP_GONE 		: 410, // Gone (The requested resource is no longer available at the server and no forwarding address is known)
		HTTP_IN_SERR 	: 500, // Internal Server Error
		HTTP_NOT_IMPL 	: 501, // Not Implemented
		HTTP_NO_SERV 	: 503, // Service Unavailable
		HTTP_PERM_DEN 	: 550, // Permission Denied
		HTTP_SESS_EXP   : 440  // HTTP Session timeout/expired
	},
	HTTP_ERR_MSGS : {
		INT_SRV_ERR		: 'There is a internal server error, please try again. If you see it often, report it to admin admin@gmail.com',
		SRV_UN_AVAIL	: 'This Service is temporarily UN-Available',
		INV_LOGIN		: 'Invalid Username/Password',
		SESS_EXP 		: 'Your Session is Expired, Please Login again',
		BAD_REQ_MSG 	: 'Invalid Request, Please Login again',
		BAD_PARAM_MSG 	: 'Invalid Parameters',
		NO_TOKEN_MSG 	: 'No token supplied, Please login again'
	},
	HTTP_HEAD_CNT : {
		USR_ID : 'User_Id'
	}
};