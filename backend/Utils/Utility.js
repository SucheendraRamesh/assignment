'use strict'
const 	bpay 	 = require('bpay'),
		crypto 	 = require('crypto'),
		jwt 	 = require('njwt'),
		minLen 	 = 6,
		maxLen 	 = 9;


const getUniqueId = () => {
	return (bpay.generateBpayCRN(bpay.getRandomInt(minLen, maxLen)) * 1);
};


/**
 * This is the common function used to send respond to UI
 *
 */
const sendResponse = (res, error, success) => {
	//if there are any error respond back with respective error
	//code & message else with success code & msg
	if (error) {
		if (error.eCode && error.eMsg)
			res.status(error.eCode).send({ error: error.eMsg });
		else if (!error.eCode && error.eMsg)
			res.send({ error: error.eMsg });
	} else if (success) {
		console.log('Utilities.sendResponse '+success.code);
		if (success.code && success.msg)
			res.status(success.code).send({ success: true, 
				msg: success.msg, data: success.data});
		else if (!success.code && success.msg)
			res.send({ success: true, msg: success.msg, data: success.data });
	}
}




module.exports.getUniqueId 		   	= getUniqueId;
module.exports.sendResponse 		= sendResponse;

