const express = require('express');

const mongoose = require('mongoose');

// instantiating the express
const app = express();

// getting the port from package.json
const port = process.env.PORT || 6000;

const bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());

//db connection
const dbConnection = mongoose.connect('mongodb://localhost/altimetrik', (err, success) => {
  if(err){
    console.log('Error in connection ');
  }
  else{
    console.log('Connection Successful');
  }
});

'use strict'

const defController = require('./Controllers/DefinitionController');
const accController = require('./Controllers/AccessController');
const othController = require('./Controllers/OthersController');

//INIT  THE GUI PATH
 app.use("/",express.static(__dirname + '/build'));

//Definition Component
app.post('/api/store', defController.insertData);


//Access Component 
app.post('/api/storeaccessdata', accController.insertData);
app.get('/api/getaccessdata', accController.getAllData);
app.delete('/api/removeaccess/:aid', accController.deleteData);

//Other Component
app.post('/api/storeothersdata', othController.insertData);


//routing logic
  // app.get('/api/brand/fetch', bmController.getData);
  // app.post('/api/brand/insert', bmController.insertData);

app.listen(port, () => {
  console.log(`listening at the port ${port}`);
});

