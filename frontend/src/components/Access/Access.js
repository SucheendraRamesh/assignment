import React, {Component} from 'react';
import Table from 'react-bootstrap/Table'
import Form from 'react-bootstrap/Form';
import DisplayData from './DisplayData';
import './Access.css';
import axios from 'axios';
import Alert from 'react-bootstrap/Alert'


class Access extends Component {

   constructor(props){
      super(props);
      this.state = {
          isRoot: true,
          isArchitect: false,
          isDeveloper: false,
          isSupportEngineer: false,
          isProducer: false,
          isConsumer: false,
          userType: 'Root Admin',
          name: '',
          dataRecieved: false
      };
      this.getData();
  }

  dataValues;
  users = [
    {
        id: 1,
        value: 'Root Admin'
    }, {
        id: 2,
        value: 'Architect'
    }, {
        id: 3,
        value: 'Developer'
    }, {
        id: 4,
        value: 'Support Engineer'
    },
    {
      id: 5,
      value: 'Producer'
    },
    {
      id: 6,
      value: 'Consumer'
    }
    ];
  


  // React Checkboxes onChange Methods
onChangeRoot = () => {
  this.setState(initialState => ({
    isRoot: !initialState.isRoot,
  }));
}

onChangeArchitect = () => {
  this.setState(initialState => ({
    isArchitect: !initialState.isArchitect,
  }));
}

onChangeDeveloper = () => {
  this.setState(initialState => ({
    isDeveloper: !initialState.isDeveloper,
  }));
}

onChangeSupportEngineer = () => {
  this.setState(initialState => ({
    isSupportEngineer: !initialState.isSupportEngineer,
  }));
}

onChangeProducer = () => {
  this.setState(initialState => ({
    isProducer: !initialState.isProducer,
  }));
}

onChangeConsumer = () => {
  this.setState(initialState => ({
    isConsumer: !initialState.isConsumer,
  }));
}

nameChangeHandler = (event) => {
  // console.log("Name changed",event.target.value);
  let userType = event.target.value;

  if(userType == 'Root Admin'){
    this.setState({
      isRoot: true,
      isArchitect: false,
      isDeveloper: false,
      isSupportEngineer: false,
      isProducer: false,
      isConsumer:false
    })
  }
  else if(userType == 'Architect'){
    this.setState({
      isRoot: false,
      isArchitect: true,
      isDeveloper: false,
      isSupportEngineer: false,
      isProducer: false,
      isConsumer: false,
    })
  }
  else if(userType == 'Developer'){
    this.setState({
      isRoot: false,
      isArchitect: false,
      isDeveloper: true,
      isSupportEngineer: false,
      isProducer: false,
      isConsumer: false,
    })
  }
  else if(userType == 'Support Engineer'){
    this.setState({
      isRoot: false,
          isArchitect: false,
          isDeveloper: false,
          isSupportEngineer: true,
          isProducer: false,
          isConsumer: false,
    })
  }
  else if(userType == 'Producer'){
    this.setState({
      isRoot: false,
      isArchitect: false,
      isDeveloper: false,
      isSupportEngineer: false,
      isProducer: true,
      isConsumer: false,
    })
  }
  else if(userType == 'Consumer'){
    this.setState({
      isRoot: false,
      isArchitect: false,
      isDeveloper: false,
      isSupportEngineer: false,
      isProducer: false,
      isConsumer: true,
    })
  }
  this.setState({
    userType: event.target.value,
  })
}


onSubmit = (e) => {
  // console.log(this.state);
  if(this.state.name ==''){
    alert('Name is Mandatory');
    e.preventDefault();  
  }
  else{
    e.preventDefault();
    axios.post('/api/storeaccessdata',this.state)
        .then(res => {
          alert(res.data.msg);
           this.getData();
           this.renderDynamicData();
         
        }).catch(err => {
          // console.log(err.response.data);
          alert(err.response.status + ' ' + err.response.data.errors[0]);
        })
  }
  
  
}

getData = () => {
  axios.get('/api/getaccessdata').then(
    res => {
      this.dataValues = res['data']['data']['object'];
      // console.log('Data Values in Access',this.dataValues);
      if(!this.dataValues){
        return;
      }
      else{
        this.setState({
          dataRecieved:true
        })
      }
    }
  ).catch(
    err=>{
      console.log('err',err);
  });  
}


onNameChange = (event) => {
  // console.log(event.target.value);
  this.setState({
    name: event.target.value
  })
}

renderDynamicData = () => {
return(
  <span>
    {this.dataValues.map((ele,index)=>{
    return <DisplayData refresh={this.getData} data={ele} />
  })}
  </span>
)    
}

render(){

const isDataReady = this.state.dataRecieved;
// console.log('isDataReady',isDataReady);
return (
  <form onSubmit={this.onSubmit}>
    <div class="container-fluid" id="access">
      <div class="row">
          <div class="col-sm-12">
            <Alert style={{marginTop:"10px"}} variant="info">
            <h4>Add User</h4>
            </Alert>
          </div>
      </div>
      <div class="row">
          <div class="col-sm-12">
          <div class="table-responsive">
          <Table striped bordered hover>
          <thead>
            <tr>
              <th>Name</th>
              <th>Type</th>
              <th>Root Admin</th>
              <th>Architect</th>
              <th>Developer</th>
              <th>Support Engineer</th>
              <th>Producer</th>
              <th>Consumer</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>
                <input type="text" style={{width:"150px"}}
                      value={this.state.name}
                      onChange={(event) => this.onNameChange(event)}
                      className="form-control"
                    />
              </td>
              <td>
                <select style={{width:"150px"}} class="form-control" onChange={(event)=> this.nameChangeHandler(event)} >
                  {this.users.map(user => {
                    return <option>{user.value}</option>
                  })}
                </select>
              </td>
              <td>
                <input type="checkbox"
                        checked={this.state.isRoot}
                        onChange={this.onChangeRoot}
                       
                />
              </td>
              <td>
                <input type="checkbox"
                      checked={this.state.isArchitect}
                      onChange={this.onChangeArchitect}
                      
                    />
              </td>
              <td>
                <input type="checkbox"
                      checked={this.state.isDeveloper}
                      onChange={this.onChangeDeveloper}
                      
                    />
              </td>
              <td>
                <input type="checkbox"
                      checked={this.state.isSupportEngineer}
                      onChange={this.onChangeSupportEngineer}
                      
                    />
              </td>
              <td>
                <input type="checkbox"
                      checked={this.state.isProducer}
                      onChange={this.onChangeProducer}
                      
                    />
              </td>
              <td>
              <input type="checkbox"
                    checked={this.state.isConsumer}
                    onChange={this.onChangeConsumer}
                    
                  />
              </td>
              <td>
              <button className="btn btn-success">
                    Save
              </button>
              </td>
            </tr>
          </tbody>
        </Table>
          </div>
          </div>
      </div>

      <div class="row">
      <div class="col-sm-12">
          <div class="table-responsive">
          <Table striped bordered hover>
          
          </Table>
            </div>
      </div>
      </div>

      <div class="row">
      <div class="col-sm-12">
          <div class="table-responsive">
          <Table striped bordered hover>
          {
            isDataReady ? this.renderDynamicData() : <h2>Please wait</h2>
            }
          </Table>
            </div>
      </div>
      </div>
    
  </div>
  </form>


  );
 }
}

export default Access;