import React, {Component} from 'react';
import Table from 'react-bootstrap/Table';
import axios from 'axios';
import { FaTrashAlt, FaPen } from 'react-icons/fa';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col'

class DisplayData extends Component {
    
    state = {
        loadedPost: null
    }

    ele = this.props.data;
    
    // onEditHandler = (rowData) =>
    // {
    //     console.log('RowData',rowData);
    // }

    onDeleteHandler = (rowData) => {
        // console.log('rowData',rowData);
        const rowId = rowData['aid'];

        if(window.confirm('Click ok to delete ')){
            axios.delete(`/api/removeaccess/${rowId}`)
        .then( res => {
            if(!res){
                return;
            }
            else{
                this.setState({
                    loadedPost: true
                })
        
                if(this.state.loadedPost){
                    // this.props.disp();
                    this.props.refresh();
                    window.location.reload();
                    // setTimeout(()=>{
                    //     console.log('Executing after 2 seconds');
                        
                    // },2000) 
                }
            }
        })
        .catch(
            err => {
                console.log('deletion failed');
            }
        )
        }
    }
    render(){  
        // console.log('Inside dispdata component',this.ele);       
        return(
            <tbody>
                <tr>
                    <td>
                    <input style={{border:0,backgroundColor:"#ECECEC",width:"150px"}} type="text"
                            value={this.ele.name}
                            className="form-control"
                            disabled/>
                    </td>
                    <td>
                    <input type="text"
                    style={{border:0,backgroundColor:"#ECECEC",width:"150px"}}
                            value={this.ele.userType}
                            className="form-control"
                            disabled
                            />
                    </td>
                    <td style={{paddingLeft:"30px"}}>
                    <label className="form-check-label">
                        <input type="checkbox"
                            checked={this.ele.isRoot}
                            
                            disabled
                        />Root
                        </label>
                    </td>
                    <td  style={{paddingLeft:"30px"}}>
                    <label className="form-check-label">
                        <input type="checkbox"
                        checked={this.ele.isArchitect}
                        disabled
                        
                        />Architect
                    </label>
                    </td>
                    <td  style={{paddingLeft:"30px"}}>
                    <label className="form-check-label">
                        <input type="checkbox"
                        checked={this.ele.isDeveloper}
                        disabled
                        
                        />Developer
                    </label>
                    </td>
                    <td  style={{paddingLeft:"30px"}}>
                    <label className="form-check-label">
                        <input type="checkbox"
                        checked={this.ele.isSupportEngineer}
                        disabled
                       
                        />SupportEngineer
                    </label>
                    </td>
                    <td  style={{paddingLeft:"30px"}}> 
                    <label className="form-check-label">
                        <input type="checkbox"
                        checked={this.ele.isProducer}
                        disabled
                        
                        />
                    Producer
                    </label>
                    </td>
                    <td  style={{paddingLeft:"30px"}}> 
                    <label className="form-check-label">
                        <input type="checkbox"
                        checked={this.ele.isConsumer}
                        disabled
                        
                        />
                    Consumer
                    </label>
                    </td>
                    {/* <td>
                        <button onClick={this.onEditHandler.bind(this, this.ele)}>
                        <FaPen/> 
                    </button>
                    </td> */}
                    <td style={{width:"20%"}}>
                    <i style={{cursor:"pointer"}} onClick={this.onDeleteHandler.bind(this, this.ele)}>
                    <FaTrashAlt/>
                    </i>
                    </td>
                </tr>
                </tbody>
        )
    }
}

export default DisplayData;