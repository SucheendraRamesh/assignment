import React, {Component} from 'react';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Division from './Division';
import Stream from './Stream';
import Type from './Type';
import axios from 'axios';
import './Definition.css';


class Definition extends Component {

    constructor(props){
        super(props);
        this.state = {
            applicationName: '',
            division: 'Division1',
            stream: 'Stream1',
            type: 'Type1',
            startDate: '',
            endDate: '',
            financialSwitch: false
        }

        this.handleSubmit = this.handleSubmit.bind(this);
    }
    
    // For application name change
    applicationNameChangeHandler = (event) => {
       this.setState({
        applicationName: event.target.value
       })
       
    }
    // For division name change
    divisionChangeHandler = (event) => {
        this.setState({
            division: event.target.value
           })
    }
    // For stream name change
    streamChangeHandler = (event) => {
        this.setState({
            stream: event.target.value
           })
    }
    // For type change
    typeChangeHandler = (event) => {
        this.setState({
            type: event.target.value
           })
    }

    //for start date
    startDateChangeHandler = (event) => {
        this.setState({
            startDate: event.target.value
           })
    }

    //for end date
    endDateChangeHandler = (event) => {
        this.setState({
            endDate: event.target.value
           })
    }

    financialSwitchHandler = () => {
        // console.log(event);
        let finSwitch = this.state.financialSwitch;
        this.setState({
            financialSwitch: !finSwitch
        })
    }

    handleSubmit(event) {
       if(!this.state.applicationName){
            alert('Please enter the application name');
            event.preventDefault();
       }
       else if(!this.state.division){
        alert('Please Select the division');
        event.preventDefault();
       }
       else if(!this.state.stream){
        alert('Please Select the stream');
        event.preventDefault();
       }
       else if(!this.state.type){
        alert('Please Select the type');
        event.preventDefault();
       }
       else if(!this.state.startDate){
        alert('Please Select the Start Date');
        event.preventDefault();
       }
       else if(!this.state.endDate){
        alert('Please Select the End Date');
        event.preventDefault();
       }
       else {
           console.log(this.state);
            event.preventDefault();
            this.save();
       }
      }


      save = () => {
          axios.post('/api/store',this.state)
          .then(res => {
            // console.log('Final Response ',res);
            alert(res.data.msg);
          }).catch(err => {
            alert(+err.response.status+" "+err.response.data.errors[0]);
            // console.log(err.response.status);
            // console.log(err.response.headers);
            // console.log(err);
          })
      }

      
    
      render(){
        
        return (
            <Form onSubmit={(event) => this.handleSubmit(event)}>
            <div class="container-fluid" id="definition">
                <div class="row">
                    <div class="col-md-12">
                        <Form.Label style={{float:"left"}}>Application Name</Form.Label>
                        <Form.Control style={{marginBottom:"10px"}} 
                        value={this.state.applicationName}
                        type="text"
                        onChange={(event) => this.applicationNameChangeHandler(event)} />
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                         {/** For division dropdown*/}
                         
                        <Division style={{marginBottom:"10px"}} 
                        changed={(event)=>this.divisionChangeHandler(event)}/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        {/** For stream dropdown*/} 
                        
                        <Stream style={{marginBottom:"10px"}} 
                        changed={(event)=>this.streamChangeHandler(event)}/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        {/** For type dropdown*/}  
                        
                        <Type style={{marginBottom:"10px"}} changed={(event)=>this.typeChangeHandler(event)}/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                    <Form.Label style={{float:"left"}}>Start Date</Form.Label>
                        <Form.Control style={{marginBottom:"10px"}}
                        value={this.state.startDate}  
                        type="date"
                        onChange={(event) => this.startDateChangeHandler(event)} />

                    </div>
                    <div class="col-md-4">
                    <Form.Label style={{float:"left"}}>End Date</Form.Label>
                        <Form.Control style={{marginBottom:"10px"}}
                        value={this.state.endDate} 
                        type="date"
                        onChange={(event) => this.endDateChangeHandler(event)} />
                    </div>
                    <div class="col-md-4">
                    <br/>
                    <Form.Check style={{float:"left"}}
                        type="switch"
                        id="custom-switch"
                        label=""
                        value={this.state.financialSwitch}
                        onClick={this.financialSwitchHandler}
                        /><Form.Label style={{float:"left"}}>Financial Restricted Application</Form.Label>
                    </div>

                    <Button style={{margin: "auto", display: "block"}} onCLick={(event)=>this.handleSubmit(event)} variant="primary" type="submit">
                    Submit
                </Button>
                </div>
        </div>
        </Form>
        )
    }
}

export default Definition;