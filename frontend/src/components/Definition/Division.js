import React from 'react';
import Form from 'react-bootstrap/Form';
import Dropdown from '../UI/Dropdown';

const Division = (props) => {

    let divisions = [
        {
            id: 1,
            value: 'Division1'
        }, {
            id: 2,
            value: 'Division2'
        }, {
            id: 3,
            value: 'Division3'
        }, {
            id: 4,
            value: 'Division4'
        }
        ];

    return (
        <div>
            <Form.Group>
                <Form.Label style={{float:"left"}}>Division</Form.Label>
                    <Form.Control as="select" onChange={props.changed}>
                    <Dropdown options={divisions}/>
                    </Form.Control>
                </Form.Group>
        </div>
    );
}

export default Division;