import React from 'react';
import Form from 'react-bootstrap/Form';
import Dropdown from '../UI/Dropdown';

const Stream = (props) => {

    let streams = [
        {
            id: 1,
            value: 'Stream1'
        }, {
            id: 2,
            value: 'Stream2'
        }, {
            id: 3,
            value: 'Stream3'
        }, {
            id: 4,
            value: 'Stream4'
        }
        ];

    return (
        <div>
            <Form.Group>
                <Form.Label style={{float:"left"}}>Streams</Form.Label>
                    <Form.Control as="select" onChange={props.changed}>
                    <Dropdown options={streams}/>
                    </Form.Control>
                </Form.Group>
        </div>
    );
}

export default Stream;