import React from 'react';
import Form from 'react-bootstrap/Form';
import Dropdown from '../UI/Dropdown';

const Type = (props) => {

    let types = [
        {
            id: 1,
            value: 'Type1'
        }, {
            id: 2,
            value: 'Type2'
        }, {
            id: 3,
            value: 'Type3'
        }, {
            id: 4,
            value: 'Type4'
        }
        ];

    return (
        <div>
            <Form.Group>
                <Form.Label style={{float:"left"}}>Type</Form.Label>
                    <Form.Control as="select" onChange={props.changed}>
                    <Dropdown options={types}/>
                    </Form.Control>
                </Form.Group>
        </div>
    );
}

export default Type;