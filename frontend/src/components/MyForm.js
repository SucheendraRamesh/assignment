import React, { Component } from 'react';
import { Tabs, Tab } from "react-bootstrap";
import Definition from './Definition/Definition';
import Navbar from 'react-bootstrap/Navbar';
import Others from './Others/Others'
import Access from './Access/Access'
import axios from 'axios';
class MyForm extends Component {
    constructor(props){
        super(props);
        this.state = {
            username: '',
            comments: '',
            selValue: 'react'

        };
    }

    onInputChange = (event) => {
        // console.log(this.state.username);
        this.setState({
           username: event.target.value
       })
       
    }

    // componentDidMount(){
    //     axios.post('/test',).then(res=>{
    //         console.log(res);
    //     });
    // }

    onAddingComments = (event) => {
        // console.log(this.state.comments);
        this.setState({
            comments: event.target.value
        })
    }
    
    onSelectingOptions = (event) => {
        //  console.log(this.state.selValue);
        this.setState({
            selValue: event.target.value
        })
    }

    // submitFormHandler = () => {
    //         console.log('Form Submitted');
    // }

    handleSubmit = event => {
        alert( `${this.state.username} ${this.state.comments} ${this.state.selValue}  `);
        event.preventDefault();
    }

    render(){
        // console.log(this.state.username);
        return (
          <div style={{width: "100%"}} className="container-fluid">
                <div style={{marginBottom: "30px"}}>
                <Navbar variant="dark" bg="dark">
                    <Navbar.Brand>Application Onboarding</Navbar.Brand>
                    <Navbar.Toggle />
                    <Navbar.Collapse className="justify-content-end">
                        <Navbar.Text>
                        New Application
                        </Navbar.Text>
                    </Navbar.Collapse>
                    </Navbar>
                </div>
                <div>
                    <Tabs defaultActiveKey="home" id="altimetril-tab">
                        <Tab eventKey="home" title="DEFINITION">
                            <Definition />
                        </Tab>
                        <Tab eventKey="access" title="ACCESS">
                            <Access />
                        </Tab>
                        <Tab eventKey="other" title="OTHER" >
                            <Others/>
                        </Tab>
                    </Tabs>
                </div>
            </div>
        );
    }
}

export default MyForm;