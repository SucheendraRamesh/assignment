import React, {Component} from 'react';
import Form from 'react-bootstrap/Form';
import axios from 'axios';
import Button from 'react-bootstrap/Button';
import './Others.css';
class Others extends Component {

    constructor(props){
        super(props);
        this.state = {
            otherData: ''
        };
    }


    otherDataHandler = (event) => {
        this.setState({
            otherData: event.target.value
        })
    }

    handleSubmit = (event) => {
        if(this.state.otherData == ''){
            alert('Some data need to be entered');
           
        }
        else{
            this.save();
            event.preventDefault();
        }
    }

    save = () => {
        axios.post('/api/storeothersdata',this.state)
          .then(res => {
            alert(res.data.msg);
          }).catch(err => {
            console.log(err);
          })
    }
    

    render(){
       return ( <div id="others">
        <Form.Group>
            <Form.Control as="textarea" value={this.state.otherData} onChange={(event) => this.otherDataHandler(event)} rows="16" />
        </Form.Group>
        <Button onClick={(event)=>this.handleSubmit(event)} variant="primary" type="submit">
                    Submit
                </Button>
    </div>);
    }
}

export default Others;