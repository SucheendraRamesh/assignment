import React from 'react';
import Alert from 'react-bootstrap/Alert'
const Alerts = ( params ) => {
            
    return (
            <Alert show={params.msg}  variant="success">
            <Alert.Heading>Data Inserted Successfully</Alert.Heading>
            </Alert>
            );
}

export default Alerts;