import React from 'react';

const Dropdown = ({ options }) => {
            
    return (
        options.map(option => 
                    <option key={option.value} value={option.value}>                                   
                    {option.value}
                    </option>)
            );
}

export default Dropdown;